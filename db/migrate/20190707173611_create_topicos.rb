class CreateTopicos < ActiveRecord::Migration[5.2]
  def change
    create_table :topicos do |t|
      t.string :titulo
      t.string :jogo
      t.string :genero
      t.text :corpo

      t.timestamps
    end
  end
end
