json.extract! comentario, :id, :titulo, :corpo, :topico_id, :usuario_id, :created_at, :updated_at
json.url comentario_url(comentario, format: :json)
