json.extract! topico, :id, :titulo, :jogo, :genero, :corpo, :created_at, :updated_at
json.url topico_url(topico, format: :json)
