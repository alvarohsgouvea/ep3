class ApplicationController < ActionController::Base
	protected
	def configure_permitted_parameters
		devise_parameter_sanitizer.permit(:sign_up) { |u| u.permit({roles: []}, :email, :password) }
	end
end
