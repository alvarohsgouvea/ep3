class ComentariosController < ApplicationController
  before_action :set_comentario, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_usuario!, except: [:show]
  # GET /comentarios
  # GET /comentarios.json
  def index
    @comentarios = Comentario.all
  end

  # GET /comentarios/1
  # GET /comentarios/1.json
  def show
  end

  # GET /comentarios/new
  def new
    if usuario_signed_in?
    @comentario = Comentario.new
  end
  end

  # GET /comentarios/1/edit
  def edit
  end

  # POST /comentarios
  # POST /comentarios.json
  def create
    if usuario_signed_in?
    @comentario = Comentario.new(comentario_params)
    respond_to do |format|
      if @comentario.save
        format.html { redirect_to topicos_path, notice: 'Comentario was successfully created.'}
        format.json { render :form}
      else
        format.html { render :new }
        format.json { render json: @comentario.errors, status: :unprocessable_entity }
      end
    end
  end
  end

  # PATCH/PUT /comentarios/1
  # PATCH/PUT /comentarios/1.json
  def update
    if usuario_signed_in?
    respond_to do |format|
      if @comentario.update(comentario_params)
        format.html { redirect_to topicos_path, notice: 'Comentario was successfully updated.' }
        format.json { render :show, status: :ok, location: @comentario }
      else
        format.html { render :edit }
        format.json { render json: @comentario.errors, status: :unprocessable_entity }
      end
    end
  end
  end

  # DELETE /comentarios/1
  # DELETE /comentarios/1.json
  def destroy
    if usuario_signed_in?
    @comentario.destroy
    respond_to do |format|
      format.html { redirect_to redirect_to topicos_path, notice: 'Comentario was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_comentario
      @comentario = Comentario.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def comentario_params
      params.require(:comentario).permit(:titulo, :corpo, :topico_id, :usuario_id)
    end
end
