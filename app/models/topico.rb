class Topico < ApplicationRecord
	belongs_to :usuario
	has_many :comentarios, dependent: :destroy
end
