Rails.application.routes.draw do
  resources :topicos do
  	resources :comentarios
  end
  
  devise_for :usuarios
  get 'front_page/index'
  get 'front_page/threads'
  get 'front_page/comments'
  get 'front_page/users'
  
  root 'front_page#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
